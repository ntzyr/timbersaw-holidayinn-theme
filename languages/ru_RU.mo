��    P      �  k         �     �  /   �     
                 
   (  	   3     =     K     S     `     n     w     �  	   �     �     �     �     �     �  .   �                    ,     @     T     `     l     s     �     �     �     �  	   �     �     �     �     �     �     �     	  
   	     	     	     &	     +	     8	     <	     A	  1   I	     {	  	   �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     
  8   .
     g
     n
     ~
     �
     �
     �
     �
     �
  	   �
  #   �
     �
           S   8     �     �     �     �  #   �  $   �  !        @     Y  #   o     �     �  
   �  
   �     �     �     �       (   #  B   L     �  (   �  "   �  2   �  (   *     S     n     �  "   �  *   �     �     �          &     :     C     Y     j     �     �  
   �  !   �     �     �     	          0     8     G  l   Y     �  +   �       )     .   C  /   r     �     �     �     �     �                  #   ?  �   c     �  )   
     4     C  ,   R       )   �     �  
   �  I   �     "         @             8      .              M       !   A   C   +   '                K   -   #   3   
               7   2   $             *   1      )           (   %   4                     :   ;                      9          N   B           0   J   	      H   I   5   G       ?   >   E   P   "       L         ,       <   &              /   =   F   O               6             D                &laquo; Previous 6 meeting rooms with rooms for up to 100 people Add New  Adults All  Bar Form Basic Info Bath Info Book the hall Booking Booking Form Booking Phone Booking: Capacity Check In Check Out Children Close Contact Contacts Panel Directory doesn't exists Directory is empty or something going wrong... Edit  Empty custom fields Empty field data Empty metabox title Empty taxonomy name Footer Menu Forms Panel Friday Front Gallery Front Images Front Page Blocks Front Page Panel Gallery Hall Form Halls Hotel Address Hotel Email Hotel Phone Hotel Section Links Map Max Adults Max Children Monday New  Next &raquo; No  Open Parent  Perfect location at the Moscow Gate metro station Primary Menu Reception Rent Restaurant Email Restaurant Phone Restaurant Service Rooms Sales Sales Email Sales Phone Saturday Search  Sizes Social Section Something goes wrong... Spacious rooms equipped with individual air conditioning Sunday Taxonomy exists Thursday Tuesday Two restaurants and two bars Update  Upload an image View  Wednesday Wi-fi in all rooms and public areas Working Hours: Project-Id-Version: TimberSaw Starter Theme
POT-Creation-Date: 2019-05-23 08:54+0300
PO-Revision-Date: 2019-05-23 08:56+0300
Last-Translator: 
Language-Team: 
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
X-Generator: Poedit 2.2.3
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: style.css
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 &laquo; Предыдущее 6 конференц-залов с вместимостью до 100 человек Добавить  Взрослые Все  Форма ресторана Базовая информация Информация о ванной Забронировать зал Бронирование Форма брони Номер бронирования Бронирование: Вместимость Заезд Выезд Дети Закрыть Контакты Панель контактов Каталог не существует Каталог пуст или что-то идет не так... Редактировать  Пустые кастомные поля Пустые данные поля Пустой заголовок метабокса Пустое имя таксономии Меню в подвале Панель форм Пятница Галерея на главной Изображение на главной Блоки главной Панель главной Галерея Форма зала Залы Адрес отеля Email отеля Телефон отеля Секция отеля Ссылки Схема Максимум взрослых Максимум детей Понедельник Новый  Следующее &raquo; Нет  Открыть Родитель  Идеальное расположение на станции метро Московские ворота Основное меню Регистрационная стойка Аренда Email ресторанной службы Номер ресторанной службы Ресторанное обслуживание Комнаты Продажи Email отдел продаж Номер продаж Суббота Поиск  Размеры Секция соц.сетей Что-то идет не так... Просторные номера, оборудованные  индивидуальной системой кондиционирования Воскресенье Таксономия существует Четверг Вторник Два ресторана и два бара Обновить  Загрузить изображение Просмотр  Среда Wi-fi во всех номерах и общественных зонах Рабочее время: 