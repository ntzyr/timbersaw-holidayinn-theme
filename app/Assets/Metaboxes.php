<?php
/**
 * Created by PhpStorm.
 * User: ntzyr
 * Date: 2019-04-02
 * Time: 12:39
 */

namespace TimberSaw\Assets;


use TimberSaw\Core\Assets;

class Metaboxes extends Assets
{
    public function adminAssets()
    {
        wp_enqueue_script('timbersaw/public/js/admin/metaboxes', $this->public . 'js/admin/metaboxes.js');
        wp_enqueue_style('timbersaw/public/css/admin/metaboxes', $this->public . 'css/metaboxes.css');
    }
}