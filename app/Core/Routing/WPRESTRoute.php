<?php
/**
 * Created by PhpStorm.
 * User: ntzyr
 * Date: 2019-03-22
 * Time: 02:14
 */

namespace TimberSaw\Core\Routing;


use Prophecy\Exception\InvalidArgumentException;

class WPRESTRoute
{
    private $_root;
    private $_url;
    private $_callback;
    private $_args;

    public function __construct($root, $url, $callback, $args = array())
    {
        $this->_root = $root;
        $this->_url = $url;
        $this->_args = $args;

        if( is_callable($callback) ) {
            $this->_callback = $callback;

            return;
        }

        try {
            $this->_setCallback($callback);
        } catch (InvalidArgumentException $e) {
            exit('Route callback is not callable!');
        }
    }

    protected function _setCallback(string $callback)
    {
        $callback = explode( "@", $callback);

        if( count($callback) == 2 && file_exists('app/Controllers/' . $callback[0] . '.php') ) {
            $this->_callback[0] = new $callback[0];
            $this->_callback[1] = $callback[1];
        } else {
            throw new InvalidArgumentException('$callback is invalid.');
        }
    }

    public function getURL()
    {
        return $this->_url;
    }

    public function getCallback()
    {
        return $this->_callback;
    }

    public function getArgs()
    {
        return $this->_args;
    }

    public function getRoot()
    {
        return $this->_root;
    }
}