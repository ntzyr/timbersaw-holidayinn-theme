<?php
/**
 * Created by PhpStorm.
 * User: ntzyr
 * Date: 2019-03-22
 * Time: 02:18
 */

namespace TimberSaw\Core\Routing;


class WPRESTRouter
{
    private static $_routes = [];
    
    public static function add(WPRESTRoute $route, $method)
    {
        self::$_routes[$route->getRoot()] = array(
            'url' => $route->getURL(),
            'settings' => array(
                'methods' => $method,
                'callback' => $route->getCallback(),
                // @todo 'permission_callback' => $route->permissionCallback,
                'args' => $route->getArgs()
            )
        );
//        register_rest_route(
//            $route->getRoot(),
//            $route->getURL(),
//            array(
//                'methods' => $method,
//                'callback' => $route->getCallback(),
//                // @todo 'permission_callback' => $route->permissionCallback,
//                'args' => $route->getArgs()
//            )
//        );
    }

    public static function get(WPRESTRoute $route )
    {
        self::add($route, 'get');
    }

    public static function post(WPRESTRoute $route )
    {
        self::add($route, 'post');
    }

    public static function run()
    {
        $routes = self::$_routes;
        add_action('rest_api_init', function() use($routes) {
            foreach( self::$_routes as $root => $route ) {
                register_rest_route(
                    $root,
                    $route['url'],
                    $route['settings']
                );
            }
        } );
    }
}