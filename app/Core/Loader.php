<?php
/**
 * Created by PhpStorm.
 * User: ntzyr
 * Date: 2019-03-28
 * Time: 21:52
 */

namespace TimberSaw\Core;

class Loader
{
    static public function themeDirectory()
    {
        return get_template_directory();
    }

    static function getFiles($directory)
    {
        if(! file_exists($directory)) throw new Exception(__('Directory doesn\'t exists', 'timbersaw-theme'));

        $dirFiles = array_diff(scandir($directory), array('..', '.'));

        if(!is_array($dirFiles) && $dirFiles == null) throw new Exception(__('Directory is empty or something going wrong...', 'timbersaw-theme'));

        return $dirFiles;
    }

    static function getClassFiles($directory) : ?array
    {
        if(! file_exists($directory)) throw new Exception(__('Directory doesn\'t exists', 'timbersaw-theme'));

        $dirFiles = array_diff(scandir($directory), array('..', '.'));

        if(!is_array($dirFiles) && $dirFiles == null) throw new Exception(__('Directory is empty or something going wrong...', 'timbersaw-theme'));

        foreach ($dirFiles as $file) {
            $file = explode('.', $file)[0];

            if( !empty($file) ) $files[] = $file;
        }

        return $files;
    }

    static public function assets()
    {
        $themeDirectory = self::themeDirectory() . '/app/Assets/';
        $files = self::getClassFiles($themeDirectory);

        foreach($files as $asset) {
            $class = "\\TimberSaw\\Assets\\$asset";
            new $class;
        }
    }

    static public function restRoutes()
    {
        $themeDirectory = self::themeDirectory() . '/routes/rest/';
        $files = self::getFiles($themeDirectory);

        foreach ($files as $file) {
            require $themeDirectory . $file;
        }
    }

    static public function customizer()
    {
        $themeDirectory = self::themeDirectory() . '/app/Customizer/';
        $files = self::getClassFiles($themeDirectory);

        if(!$files || $files == null) return;

        foreach ($files as $file) {
            $class = "\\TimberSaw\\Customizer\\$file";
            add_action('customize_register', array($class, 'register'));
            add_action('timber_context', array($class, 'context'));
        }
    }

    static public function posttypes()
    {
        $themeDirectory = self::themeDirectory() . '/app/CustomPostType/';
        $files = self::getFiles($themeDirectory);

        foreach ($files as $file) {
            require $themeDirectory . $file;
        }
    }
}