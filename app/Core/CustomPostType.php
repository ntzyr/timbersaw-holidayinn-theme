<?php
/**
 * Created by PhpStorm.
 * User: ntzyr
 * Date: 2019-03-22
 * Time: 03:24
 */

namespace TimberSaw\Core;


use http\Exception;

class CustomPostType
{
    public $postTypeName;
    public $postTypeArgs;
    public $postTypeLabels;

    public function __construct($name, array $args = [], array $labels = [])
    {
        $this->postTypeName = $name;
        $this->postTypeArgs = $args;
        $this->postTypeLabels = $labels;

        if( ! post_type_exists( $this->postTypeName ) ) {
            add_action('init', array($this, 'register'));
        }

        $this->save();
    }

    public function setPlural($name)
    {
        if( strpos($name, 'y' , -strlen($name)) ) {
            $plural = substr($name, 0, -1 );

            return $plural;
        } else {
            $plural = $name . "s";

            return $plural;
        }
    }

    public function register()
    {
        $name = ucwords( str_replace('_', ' ', $this->postTypeName) );
        $plural = $this->setPlural($name);

        $labels = array_merge(
            array(
                'name'                  => _x( $plural, 'post type general name', 'timbersaw-theme' ),
                'singular_name'         => _x( $name, 'post type singular name', 'timbersaw-theme' ),
                'add_new'               => _x( 'Add New', strtolower( $name ), 'timbersaw-theme' ),
                'add_new_item'          => __( 'Add New ' . $name, 'timbersaw-theme' ),
                'edit_item'             => __( 'Edit ' . $name, 'timbersaw-theme' ),
                'new_item'              => __( 'New ' . $name, 'timbersaw-theme' ),
                'all_items'             => __( 'All ' . $plural, 'timbersaw-theme' ),
                'view_item'             => __( 'View ' . $name, 'timbersaw-theme' ),
                'search_items'          => __( 'Search ' . $plural, 'timbersaw-theme' ),
                'not_found'             => __( 'No ' . strtolower( $plural ) . ' found', 'timbersaw-theme'),
                'not_found_in_trash'    => __( 'No ' . strtolower( $plural ) . ' found in Trash', 'timbersaw-theme'),
                'parent_item_colon'     => '',
                'menu_name'             => __( $plural, 'timbersaw-theme' )
            ),
            $this->postTypeLabels
        );

        $args = array_merge(
            array(
                'label'                 => $plural,
                'labels'                => $labels,
                'public'                => true,
                'publicly_queryable'    => true,
                'show_ui'               => true,
                'supports'              => array( 'title', 'editor', 'thumbnail' ),
                'show_in_nav_menus'     => true,
                '_builtin'              => false,
                'has_archive'           => true
            ),
            $this->postTypeArgs
        );

        register_post_type($this->postTypeName, $args);
    }

    public function addTaxonomy($taxonomyName, array $taxonomyArgs = [], array $taxonomyLabel = [])
    {
        if( $taxonomyName == null ) throw new \Exception(__('Empty taxonomy name', 'timbersaw-theme'));

        $posttype = $this->postTypeName;
        $taxonomyID = strtolower( str_replace( ' ', '_', $taxonomyName ) );

        if( taxonomy_exists($taxonomyID) ) throw new \Exception(__('Taxonomy exists', 'timbersaw-theme'));

        $name = ucwords( str_replace( array('_', '-'), ' ', $taxonomyName ) );
        $plural = $this->setPlural($name);

        $labels = array_merge(
            array(
                'name'                  => _x( $plural, 'taxonomy general name', 'timbersaw-theme'),
                'singular_name'         => _x( $name, 'taxonomy singular name', 'timbersaw-theme' ),
                'search_items'          => __( 'Search ' . $plural, 'timbersaw-theme' ),
                'all_items'             => __( 'All ' . $plural, 'timbersaw-theme' ),
                'parent_item'           => __( 'Parent ' . $name, 'timbersaw-theme' ),
                'parent_item_colon'     => __( 'Parent ' . $name . ':', 'timbersaw-theme' ),
                'edit_item'             => __( 'Edit ' . $name, 'timbersaw-theme' ),
                'update_item'           => __( 'Update ' . $name, 'timbersaw-theme' ),
                'add_new_item'          => __( 'Add New ' . $name, 'timbersaw-theme' ),
                'new_item_name'         => __( 'New ' . $name . ' Name', 'timbersaw-theme' ),
                'menu_name'             => __( $name ),
            ),
            $taxonomyLabel
        );

        $args = array_merge(
            array(
                'label'                 => $plural,
                'labels'                => $labels,
                'public'                => true,
                'publicly_queryable'    => true,
                'show_ui'               => true,
                'show_in_nav_menus'     => true,
                '_builtin'              => false
            ),
            $taxonomyArgs
        );

        add_action(
            'init',
            function() use($taxonomyID, $posttype, $args) {
                register_taxonomy($taxonomyID, $posttype, $args);
            }
        );
    }

    public function addMetabox(array $title, array $fields, array $visible = null, $context = 'normal', $priority = 'default')
    {
        if( $title == null ) throw new \Exception(__('Empty metabox title', 'timbersaw-theme'));
        if( $visible == null ) $visible[] = $this->postTypeName;

        $posttype = $this->postTypeName;

        $boxID = $title['id'];
        $boxTitle = $title['title'];

        global $custom_fields;
        $custom_fields[$boxID] = $fields;

        add_action( 'admin_init',
            function() use($boxID, $boxTitle, $posttype, $visible, $context, $priority, $fields) {
                add_meta_box(
                    $boxID,
                    $boxTitle,
                    function($post, $data) use($boxID) {
                        global $post;

//                        wp_nonce_field( plugin_basename( __FILE__ ), 'timbersaw-posttype' );

                        $customFields = $data['args'][$boxID];

//                        $meta = get_post_meta( $post->ID, $boxID, true );

                        if( $customFields == null ) return new \Exception(__('Empty custom fields', 'timbersaw-theme'));

                        foreach($customFields as $field) {
                            new MetaboxField($boxID, $field);
                        }
                    },
                    $visible,
                    $context,
                    $priority,
                    array(
                        $boxID => $fields
                    )
                );
            }
        );
    }

    public function save()
    {
        $posttype = $this->postTypeName;

        add_action( 'save_post',
            function() use($posttype) {
                if( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return;

                global $post;

                if(isset( $_POST ) && isset( $post->ID ) && get_post_type( $post->ID ) == $posttype) return new \Exception(__('Something goes wrong...', 'timbersaw-theme'));

                global $custom_fields;

                foreach ($custom_fields as $boxID => $fields) {
                    $value = $_POST[$boxID];
                    update_post_meta($post->ID, $boxID, $value);
                }
            }
        );
    }
}