<?php
/**
 * Created by PhpStorm.
 * User: ntzyr
 * Date: 2019-03-21
 * Time: 23:51
 */

namespace TimberSaw\Core;


class Assets
{
    public $public;

    public function __construct()
    {
        $this->setPublic();

        if( method_exists($this, 'frontAssets') ) {
            add_action('wp_enqueue_scripts', array($this, 'frontAssets'));
        }
        if( method_exists($this, 'adminAssets') ) {
            add_action('admin_enqueue_scripts', array($this, 'adminAssets'));
        }
        if( method_exists($this, 'localize') ) {
            $this->localize();
        }
    }

    public function setPublic()
    {
        $this->public = get_template_directory_uri() . '/public/';
    }
}