<?php
/**
 * Created by PhpStorm.
 * User: ntzyr
 * Date: 2019-04-02
 * Time: 11:46
 */

$hall = new \TimberSaw\Core\CustomPostType('hall', array(), array(
    'name'                  => _x( 'Конференц-залы', 'post type general name', 'timbersaw-theme' ),
    'singular_name'         => _x( 'Конференц-зал', 'post type singular name', 'timbersaw-theme' ),
    'add_new'               => _x( 'Добавить', 'Конференц-залы', 'timbersaw-theme' ),
    'add_new_item'          => __( 'Добавить конференц-зал', 'timbersaw-theme' ),
    'edit_item'             => __( 'Редактировать конференц-зал', 'timbersaw-theme' ),
    'new_item'              => __( 'Новый конференц-зал', 'timbersaw-theme' ),
    'all_items'             => __( 'Все конференц-залы', 'timbersaw-theme' ),
    'view_item'             => __( 'Просмотр конференц-зала', 'timbersaw-theme' ),
    'search_items'          => __( 'Искать конференц-зал', 'timbersaw-theme' ),
    'not_found'             => __( 'Не найдено конференц-залов', 'timbersaw-theme'),
    'not_found_in_trash'    => __( 'Не найдено конференц-залов в корзине', 'timbersaw-theme'),
    'parent_item_colon'     => '',
    'menu_name'             => __( 'Конференц-залы', 'timbersaw-theme' )
));
$hall->addTaxonomy('stages', array(), array(
    'name'                  => _x( 'Этажи', 'taxonomy general name', 'timbersaw-theme'),
    'singular_name'         => _x( 'Этаж', 'taxonomy singular name', 'timbersaw-theme' ),
    'search_items'          => __( 'Искать этаж', 'timbersaw-theme' ),
    'all_items'             => __( 'Все этажи', 'timbersaw-theme' ),
    'parent_item'           => __( 'Родительский этаж', 'timbersaw-theme' ),
    'parent_item_colon'     => __( 'Родительский этаж', 'timbersaw-theme' ),
    'edit_item'             => __( 'Редактировать этаж', 'timbersaw-theme' ),
    'update_item'           => __( 'Обновить этаж', 'timbersaw-theme' ),
    'add_new_item'          => __( 'Добавить этаж', 'timbersaw-theme' ),
    'new_item_name'         => __( 'Добавить этаж', 'timbersaw-theme' ),
    'menu_name'             => __( 'Этажи' ),
));
$hall->addMetabox(
    array(
        'id' => 'hall_attachments',
        'title' => __('Файлы конференц-зала', 'timbersaw-theme')
    ),
    array(
        array(
            'id' => 'gallery',
            'label' => __('Галерея конференц-зала', 'timbersaw-theme'),
            'type' => 'files'
        ),
    )
);
$hall->addMetabox(
    array(
        'id' => 'hall_info',
        'title' => __('Информация о конференц-зале', 'timbersaw-theme')
    ),
    array(
        array(
            'id' => 'info',
            'label' => __('Базовая информация', 'timbersaw-theme'),
            'type' => 'icon-text-group'
        ),
        array(
            'id' => 'sizes',
            'label' => __('Размеры', 'timbersaw-theme'),
            'type' => 'input-group'
        ),
        array(
            'id' => 'capacity',
            'label' => __('Вместимость', 'timbersaw-theme'),
            'type' => 'input-group'
        ),
        array(
            'id' => 'rent',
            'label' => __('Аренда', 'timbersaw-theme'),
            'type' => 'input-group'
        ),
        array(
            'id' => 'map',
            'label' => __('Карта', 'timbersaw-theme'),
            'type' => 'file'
        )
    )
);