<?php

$page = new \TimberSaw\Core\CustomPostType('page');

$page->addMetabox(
    array(
        'id' => 'page_additional',
        'title' => __('Дополнительная информация', 'timbersaw-theme')
    ),
    array(
        array(
            'id' => 'gallery',
            'label' => __('Галерея', 'timbersaw-theme'),
            'type' => 'files'
        ),
        array(
            'id' => 'files',
            'label' => __('Файлы', 'timbersaw-theme'),
            'type' => 'files'
        )
    )
);