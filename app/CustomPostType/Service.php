<?php
/**
 * Created by PhpStorm.
 * User: ntzyr
 * Date: 2019-04-02
 * Time: 11:47
 */

$service = new \TimberSaw\Core\CustomPostType('service', array(), array(
    'name'                  => _x( 'Услуги', 'post type general name', 'timbersaw-theme' ),
    'singular_name'         => _x( 'Услуга', 'post type singular name', 'timbersaw-theme' ),
    'add_new'               => _x( 'Добавить', 'услугу', 'timbersaw-theme' ),
    'add_new_item'          => __( 'Добавить услугу', 'timbersaw-theme' ),
    'edit_item'             => __( 'Редактировать услугу', 'timbersaw-theme' ),
    'new_item'              => __( 'Новая услуга', 'timbersaw-theme' ),
    'all_items'             => __( 'Все услуги', 'timbersaw-theme' ),
    'view_item'             => __( 'Просмотр услуги', 'timbersaw-theme' ),
    'search_items'          => __( 'Искать услугу', 'timbersaw-theme' ),
    'not_found'             => __( 'Не найдено услуг', 'timbersaw-theme'),
    'not_found_in_trash'    => __( 'Не найдено услуг в корзине', 'timbersaw-theme'),
    'parent_item_colon'     => '',
    'menu_name'             => __( 'Услуги', 'timbersaw-theme' )
));
$service->addTaxonomy('service_group', array(), array(
    'name'                  => _x( 'Группа услуг', 'taxonomy general name', 'timbersaw-theme'),
    'singular_name'         => _x( 'Группа услуг', 'taxonomy singular name', 'timbersaw-theme' ),
    'search_items'          => __( 'Искать группу', 'timbersaw-theme' ),
    'all_items'             => __( 'Все группы', 'timbersaw-theme' ),
    'parent_item'           => __( 'Родительская группа', 'timbersaw-theme' ),
    'parent_item_colon'     => __( 'Родительская группа ', 'timbersaw-theme' ),
    'edit_item'             => __( 'Редактировать группу', 'timbersaw-theme' ),
    'update_item'           => __( 'Обновить группу', 'timbersaw-theme' ),
    'add_new_item'          => __( 'Добавить группу', 'timbersaw-theme' ),
    'new_item_name'         => __( 'Добавить группу', 'timbersaw-theme' ),
    'menu_name'             => __( 'Группа услуг' ),
));
$service->addMetabox(
    array(
        'id' => 'service_info',
        'title' => __('Информация об услуге', 'timbersaw-theme')
    ),
    array(
        array(
            'id' => 'files',
            'label' => __('Приложения услуги', 'timbersaw-theme'),
            'type' => 'files'
        )
    )
);