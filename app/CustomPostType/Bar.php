<?php
/**
 * Created by PhpStorm.
 * User: ntzyr
 * Date: 2019-04-02
 * Time: 11:30
 */

$bar = new \TimberSaw\Core\CustomPostType('bar', array(
    'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' )
), array(
    'name'                  => _x( 'Рестораны', 'post type general name', 'timbersaw-theme' ),
    'singular_name'         => _x( 'Ресторан', 'post type singular name', 'timbersaw-theme' ),
    'add_new'               => _x( 'Добавить', 'Ресторан', 'timbersaw-theme' ),
    'add_new_item'          => __( 'Добавить ресторан', 'timbersaw-theme' ),
    'edit_item'             => __( 'Редактировать ресторан', 'timbersaw-theme' ),
    'new_item'              => __( 'Новый ресторан', 'timbersaw-theme' ),
    'all_items'             => __( 'Все рестораны', 'timbersaw-theme' ),
    'view_item'             => __( 'Просмотр ресторана', 'timbersaw-theme' ),
    'search_items'          => __( 'Искать ресторан', 'timbersaw-theme' ),
    'not_found'             => __( 'Не найдено ресторанов', 'timbersaw-theme'),
    'not_found_in_trash'    => __( 'Не найдено ресторанов в корзине', 'timbersaw-theme'),
    'parent_item_colon'     => '',
    'menu_name'             => __( 'Рестораны', 'timbersaw-theme' )
));
$bar->addMetabox(
    array(
        'id' => 'bar_info',
        'title' => __('Информация о ресторане', 'timbersaw-theme')
    ),
    array(
        array(
            'id' => 'gallery',
            'label' => __('Галерея ресторана', 'timbersaw-theme'),
            'type' => 'files'
        ),
        array(
            'id' => 'bar_files',
            'label' => __('Меню ресторана', 'timbersaw-theme'),
            'type' => 'files'
        ),
        array(
            'id' => 'bar_working',
            'label' => __('Рабочие часы', 'timbersaw-theme'),
            'type' => 'working-hours'
        )
    )
);