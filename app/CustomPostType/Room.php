<?php
/**
 * Created by PhpStorm.
 * User: ntzyr
 * Date: 2019-04-02
 * Time: 09:39
 */

$room = new \TimberSaw\Core\CustomPostType('room', array(), array(
    'name'                  => _x( 'Номера', 'post type general name', 'timbersaw-theme' ),
    'singular_name'         => _x( 'Номер', 'post type singular name', 'timbersaw-theme' ),
    'add_new'               => _x( 'Добавить', 'номер', 'timbersaw-theme' ),
    'add_new_item'          => __( 'Добавить номер', 'timbersaw-theme' ),
    'edit_item'             => __( 'Редактировать номер', 'timbersaw-theme' ),
    'new_item'              => __( 'Новый номер', 'timbersaw-theme' ),
    'all_items'             => __( 'Все номера', 'timbersaw-theme' ),
    'view_item'             => __( 'Просмотр номера', 'timbersaw-theme' ),
    'search_items'          => __( 'Искать номер', 'timbersaw-theme' ),
    'not_found'             => __( 'Не найдено номеров', 'timbersaw-theme'),
    'not_found_in_trash'    => __( 'Не найдено номеров в корзине', 'timbersaw-theme'),
    'parent_item_colon'     => '',
    'menu_name'             => __( 'Номера', 'timbersaw-theme' )
));
$room->addMetabox(
    array(
        'id' => 'room_meta',

        'title' => __('Приложения номера', 'timbersaw-theme')
    ),
    array(
        array(
            'id' => 'gallery',
            'label' => __('Галерея', 'timbersaw-theme'),
            'type' => 'files'
        ),
        array(
            'id' => 'files',
            'label' => __('Файлы', 'timbersaw-theme'),
            'type' => 'files'
        )
    )
);
$room->addMetabox(
    array(
        'id' => 'room_basic',
        'title' => __('Базовые особенности', 'timbersaw-theme')
    ),
    array(
        array(
            'id' => 'features',
            'type' => 'icon-text-group'
        )
    )
);
$room->addMetabox(
    array(
        'id' => 'room_bath',
        'title' => __('Ванная комната', 'timbersaw-theme')
    ),
    array(
        array(
            'id' => 'features',
            'type' => 'icon-text-group'
        )
    )
);
$room->addMetabox(
    array(
        'id' => 'room_biz',
        'title' => __('Бизнес-особенности', 'timbersaw-theme')
    ),
    array(
        array(
            'id' => 'features',
            'type' => 'icon-text-group'
        )
    )
);

$room->addMetabox(
    array(
        'id' => 'room_preview',
        'title' => __('Превью номера', 'timbersaw-theme')
    ),
    array(
        array(
            'id' => 'fields',
            'type' => 'input-group'
        )
    )
);