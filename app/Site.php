<?php
/**
 * Created by PhpStorm.
 * User: ntzyr
 * Date: 2019-03-29
 * Time: 16:54
 */

namespace TimberSaw;

use Timber\Image;
use Timber\Menu;
use \Timber\Timber;

class Site extends \Timber\Site {
    public function __construct()
    {
        add_action('after_setup_theme', array($this, 'themeSupports'));
        add_action('after_setup_theme', array($this, 'themeMenus'));
        add_action('after_setup_theme', array($this, 'textdomain'));
        add_action('widgets_init', array($this, 'sidebars'));
        add_action('timber_context', array($this, 'addContext'));
        
        parent::__construct();
    }

    public function sidebars()
    {
        
    }

    public function textdomain()
    {
        load_theme_textdomain('timbersaw-theme', get_template_directory() . '/languages');
    }

    public function themeSupports()
    {
        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        add_theme_support('custom-logo', array(
            'height' => 70,
            'width' => 240,
            'flex-height' => true,
            'flex-width'  => true,
        ));

        /*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
        add_theme_support('title-tag');

        /*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
        add_theme_support('post-thumbnails');

        /*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
        add_theme_support(
            'html5',
            array(
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
            )
        );

        /*
		 * Enable support for Post Formats.
		 *
		 * See: https://codex.wordpress.org/Post_Formats
		 */
        add_theme_support(
            'post-formats',
            array(
                'aside',
                'image',
                'video',
                'quote',
                'link',
                'gallery',
                'audio',
            )
        );

        add_theme_support('menus');
    }

    public function themeMenus()
    {
        register_nav_menu('primary_menu', __('Primary Menu', 'timbersaw-theme'));
        register_nav_menu('footer_menu', __('Footer Menu', 'timbersaw-theme'));
    }

    public function addContext($context)
    {
        $context['primary_menu'] = new Menu('primary_menu');
        $context['footer_menu'] = new Menu('footer_menu');

        $context['lang']['current'] = pll_current_language('name');
        $context['lang']['langs'] = pll_the_languages(array('raw'=>1));

        $context['custom_logo'] = new Image(get_theme_mod('custom_logo'));

        return $context;
    }
}