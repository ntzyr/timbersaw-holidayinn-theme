<?php


namespace TimberSaw\Customizer;


class Forms
{
    public function register($wp_customize)
    {
        $wp_customize->add_setting('hall_form');
        $wp_customize->add_setting('bar_form');

        $wp_customize->add_panel('form_panel', array(
            'title' => __('Forms Panel', 'timbersaw-theme')
        ));

        $wp_customize->add_section('forms_section', array(
            'title' => __('Forms Panel', 'timbersaw-theme'),
            'panel' => 'form_panel'
        ));

        $forms = get_posts( array('post_type' => 'wpcf7_contact_form', 'posts_per_page' => -1) );

        if( $forms ) {
            $choices['null'] = __('Без формы', 'timbersaw-theme');
            foreach( $forms as $form) {
                $choices[$form->ID] = $form->post_title;
            }
        }

        $wp_customize->add_control('hall_form', array(
            'type' => 'select',
            'section' => 'forms_section',
            'label' => __('Hall Form', 'timbersaw-theme'),
            'choices' => $choices
        ));

        $wp_customize->add_control('bar_form', array(
            'type' => 'select',
            'section' => 'forms_section',
            'label' => __('Bar Form', 'timbersaw-theme'),
            'choices' => $choices
        ));
    }

    public function context($context)
    {
        $context['forms']['hall_form'] = get_theme_mod('hall_form') ? do_shortcode("[contact-form-7 id='" . get_theme_mod('hall_form') . "']") : null;
        $context['forms']['bar_form'] = get_theme_mod('bar_form') ? do_shortcode("[contact-form-7 id='" . get_theme_mod('bar_form') . "']") : null;

        return $context;
    }
}