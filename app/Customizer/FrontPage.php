<?php


namespace TimberSaw\Customizer;


use Timber\Image;

class FrontPage
{
    public function register($wp_customize)
    {
        $wp_customize->add_setting('banner_img');
        $wp_customize->add_setting('front_gallery');
        $wp_customize->add_setting('front_imgs');
        $wp_customize->add_setting('max_adults');
        $wp_customize->add_setting('max_children');

        $wp_customize->add_panel('front_panel', array(
            'title' => __('Front Page Panel', 'timbersaw-theme')
        ));

        $wp_customize->add_section('block_section', array(
            'title' => __('Front Page Blocks', 'timbersaw-theme'),
            'panel' => 'front_panel'
        ));

        $wp_customize->add_control(new \WP_Customize_Image_Control(
            $wp_customize,
            'banner_img',
            array(
                'label' => __('Upload an image', 'timbersaw-theme'),
                'section' => 'block_section',
                'settings' => 'banner_img',
            )
        ));
        $wp_customize->add_control(new \CustomizeImageGalleryControl\Control(
            $wp_customize,
            'front_gallery',
            array(
                'label' => __('Front Gallery', 'timbersaw-theme'),
                'section' => 'block_section',
                'settings' => 'front_gallery',
                'type' => 'image_gallery',
            )
        ));
        $wp_customize->add_control(new \CustomizeImageGalleryControl\Control(
            $wp_customize,
            'front_imgs',
            array(
                'label' => __('Front Images', 'timbersaw-theme'),
                'section' => 'block_section',
                'settings' => 'front_imgs',
                'type' => 'image_gallery',
            )
        ));

        $wp_customize->add_section('booking_section', array(
            'title' => __('Booking Form', 'timbersaw-theme'),
            'panel' => 'front_panel'
        ));

        $wp_customize->add_control('max_adults', array(
            'type' => 'number',
            'section' => 'booking_section',
            'label' => __('Max Adults', 'timbersaw-theme'),
            'settings' => 'max_adults'
        ));
        $wp_customize->add_control('max_children', array(
            'type' => 'number',
            'section' => 'booking_section',
            'label' => __('Max Children', 'timbersaw-theme'),
            'settings' => 'max_children'
        ));
    }

    public function context($context)
    {
        $context['front_page']['banner'] = new Image(get_theme_mod('banner_img'));
        if(get_theme_mod('front_gallery')) {
            foreach (get_theme_mod('front_gallery') as $slide) {
                $context['front_page']['gallery'][] = new Image($slide);
            }
        }
        if(get_theme_mod('front_imgs')) {
            foreach (get_theme_mod('front_imgs') as $img) {
                $context['front_page']['imgs'][] = new Image($img);
            }
        }

        $context['booking']['max_adults'] = get_theme_mod('max_adults');
        $context['booking']['max_children'] = get_theme_mod('max_children');

        return $context;
    }
}