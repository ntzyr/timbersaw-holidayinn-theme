<?php
/**
 * Created by PhpStorm.
 * User: ntzyr
 * Date: 2019-04-10
 * Time: 10:58
 */

namespace TimberSaw\Customizer;


class Contacts
{
    public function register($wp_customize)
    {
        $wp_customize->add_setting('phone');
        $wp_customize->add_setting('booking_phone');
        $wp_customize->add_setting('sales_phone');
        $wp_customize->add_setting('restaurant_phone');
        $wp_customize->add_setting('reception');
        $wp_customize->add_setting('address');
        $wp_customize->add_setting('map');
        $wp_customize->add_setting('email');
        $wp_customize->add_setting('sales_email');
        $wp_customize->add_setting('restaurant_email');
        $wp_customize->add_setting('vk');
        $wp_customize->add_setting('facebook');
        $wp_customize->add_setting('google');
        $wp_customize->add_setting('instagram');

        $wp_customize->add_panel('contacts_panel', array(
            'title' => __('Contacts Panel', 'timbersaw-theme')
        ));

        $wp_customize->add_section('hotel_section', array(
            'title' => __('Hotel Section', 'timbersaw-theme'),
            'panel' => 'contacts_panel'
        ));

        $wp_customize->add_control('phone', array(
            'type' => 'text',
            'section' => 'hotel_section',
            'label' => __('Hotel Phone', 'timbersaw-theme'),
            'settings' => 'phone'
        ));
        $wp_customize->add_control('booking_phone', array(
            'type' => 'text',
            'section' => 'hotel_section',
            'label' => __('Booking Phone', 'timbersaw-theme'),
            'settings' => 'booking_phone'
        ));
        $wp_customize->add_control('sales_phone', array(
            'type' => 'text',
            'section' => 'hotel_section',
            'label' => __('Sales Phone', 'timbersaw-theme'),
            'settings' => 'sales_phone'
        ));
        $wp_customize->add_control('restaurant_phone', array(
            'type' => 'text',
            'section' => 'hotel_section',
            'label' => __('Restaurant Phone', 'timbersaw-theme'),
            'settings' => 'restaurant_phone'
        ));
        $wp_customize->add_control('reception', array(
            'type' => 'text',
            'section' => 'hotel_section',
            'label' => __('Reception', 'timbersaw-theme'),
            'settings' => 'reception'
        ));
        $wp_customize->add_control('address', array(
            'type' => 'text',
            'section' => 'hotel_section',
            'label' => __('Hotel Address', 'timbersaw-theme'),
            'settings' => 'address'
        ));
        $wp_customize->add_control('email', array(
            'type' => 'email',
            'section' => 'hotel_section',
            'label' => __('Hotel Email', 'timbersaw-theme'),
            'settings' => 'email'
        ));
        $wp_customize->add_control('sales_email', array(
            'type' => 'email',
            'section' => 'hotel_section',
            'label' => __('Sales Email', 'timbersaw-theme'),
            'settings' => 'sales_email'
        ));
        $wp_customize->add_control('restaurant_email', array(
            'type' => 'email',
            'section' => 'hotel_section',
            'label' => __('Restaurant Email', 'timbersaw-theme'),
            'settings' => 'restaurant_email'
        ));
        $wp_customize->add_control('map', array(
            'type' => 'textarea',
            'section' => 'hotel_section',
            'label' => __('Map', 'timbersaw-theme'),
            'settings' => 'map'
        ));

        $wp_customize->add_section('social_section', array(
            'title' => __('Social Section', 'timbersaw-theme'),
            'panel' => 'contacts_panel'
        ));

        $wp_customize->add_control('vk', array(
            'type' => 'url',
            'section' => 'social_section',
            'label' => __('VK', 'timbersaw-theme'),
            'settings' => 'vk'
        ));
        $wp_customize->add_control('facebook', array(
            'type' => 'url',
            'section' => 'social_section',
            'label' => __('Facebook', 'timbersaw-theme'),
            'settings' => 'facebook'
        ));
        $wp_customize->add_control('google', array(
            'type' => 'url',
            'section' => 'social_section',
            'label' => __('Google +', 'timbersaw-theme'),
            'settings' => 'google'
        ));
        $wp_customize->add_control('instagram', array(
            'type' => 'url',
            'section' => 'social_section',
            'label' => __('Instagram', 'timbersaw-theme'),
            'settings' => 'instagram'
        ));
    }

    public function context($context)
    {
        $context['phone'] = get_theme_mod('phone');
        $context['booking_phone'] = get_theme_mod('booking_phone');
        $context['sales_phone'] = get_theme_mod('sales_phone');
        $context['restaurant_phone'] = get_theme_mod('restaurant_phone');
        $context['reception'] = get_theme_mod('reception');
        $context['address'] = get_theme_mod('address');
        $context['email'] = get_theme_mod('email');
        $context['sales_email'] = get_theme_mod('sales_email');
        $context['restaurant_email'] = get_theme_mod('restaurant_email');

        $context['map'] = get_theme_mod('map');

        $context['vk'] = get_theme_mod('vk');
        $context['facebook'] = get_theme_mod('facebook');
        $context['google'] = get_theme_mod('google');
        $context['instagram'] = get_theme_mod('instagram');

        return $context;
    }
}