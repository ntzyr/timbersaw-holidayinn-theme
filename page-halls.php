<?php
/**
 * Created by PhpStorm.
 * User: ntzyr
 * Date: 2019-03-29
 * Time: 14:17
 */

$terms = get_terms(array(
    'taxonomy' => 'stages',
    'orderby' => 'id'
));

$context = Timber::get_context();
$context['title'] = __('Halls', 'timbersaw-theme');
if($terms) {
    foreach($terms as $term) {
        $context['terms'][] = new \Timber\Term($term->term_id);
    }
}

Timber::render( array( '/front/page-' . $post->post_name . '.twig', '/front/page.twig' ), $context );
