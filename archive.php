<?php
/**
 * Created by PhpStorm.
 * User: ntzyr
 * Date: 2019-03-29
 * Time: 14:16
 */

$templates = array('/front/archive.twig', '/front/index.twig');

$context = Timber::get_context();

$context['categories'] = Timber::get_terms('category', array(
    'hide_empty' => true
));

$context['term_page'] = new \Timber\Term();

$context['title'] = $context['term_page']->name;
if (is_day()) {
    $context['title'] = 'Archive: ' . get_the_date('D M Y');
} else if (is_month()) {
    $context['title'] = 'Archive: ' . get_the_date('M Y');
} else if (is_year()) {
    $context['title'] = 'Archive: ' . get_the_date('Y');
} else if (is_tag()) {
    $context['title'] = single_tag_title('', false);
} else if (is_category()) {
    $context['title'] = single_cat_title('', false);
    array_unshift($templates, '/front/archive-' . get_query_var('cat') . '.twig');
} else if (is_post_type_archive()) {
    $context['title'] = post_type_archive_title('', false);
    array_unshift($templates, '/front/archive-' . get_post_type() . '.twig');
}

$context['posts'] = new Timber\PostQuery();

if( get_post_type() == 'service') {
    foreach ($context['posts'] as $post) {
        if( isset($post->custom['service_info']['files']) ) {
            foreach($post->custom['service_info']['files'] as $file) {
                $context['files'][] = new \Timber\Image($file);
            }
        }
    }
}

Timber::render($templates, $context);