<?php
/**
 * Created by PhpStorm.
 * User: ntzyr
 * Date: 2019-03-29
 * Time: 14:18
 */


$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;

if( isset($post->custom['room_meta']['gallery']) ) {
    foreach( $post->custom['room_meta']['gallery'] as $slide) {
        $context['gallery'][] = new Timber\Image($slide);
    }
} else if(isset($post->custom['hall_attachments']['gallery'])) {
    foreach( $post->custom['hall_attachments']['gallery'] as $slide) {
        $context['gallery'][] = new Timber\Image($slide);
    }
}

if( isset($post->custom['room_basic']['features']) ) {
    foreach($post->custom['room_basic']['features'] as $feature) {
        $context['basic_info'][] = array(
            'icon' => $feature['icon'],
            'value' => $feature['value']
        );
    }
} else if( isset($post->custom['hall_info']['info']) ) {
    foreach($post->custom['hall_info']['info'] as $feature) {
        $context['basic_info'][] = array(
            'icon' => $feature['icon'],
            'value' => $feature['value']
        );
        $context['map'] = new Timber\Image($post->custom['hall_info']['map']);
    }
}

if( isset($post->custom['room_bath']['features']) ) {
    foreach($post->custom['room_bath']['features'] as $feature) {
        $context['room_bath'][] = array(
            'icon' => $feature['icon'],
            'value' => $feature['value']
        );
    }
}

if( isset($post->custom['bar_info']['bar_files'])) {
    $context['files'] = $post->custom['bar_info']['bar_files'];
} else if(isset($post->custom['page_additional']['files'])) {
    $context['files'] = $post->custom['page_additional']['files'];
} else if(isset($post->custom['service_info']['files'])) {
    $context['files'] = $post->custom['service_info']['files'];
}

if( isset($post->custom['bar_info']['bar_working'])) {
    $context['days'] = $post->custom['bar_info']['bar_working'];
}

if ( post_password_required( $post->ID ) ) {
    Timber::render( '/front/single-password.twig', $context );
} else {
    Timber::render( array( '/front/single-' . $post->ID . '.twig', '/front/single-' . $post->post_type . '.twig', '/front/single.twig' ), $context );
}
