window.$ = window.jQuery = require('jquery')
window.Popper = require('popper.js')
require('bootstrap')
import 'slick-carousel'
require('air-datepicker')
import 'selectize'
require('inputmask')
require('ion-rangeslider')

$(document).ready(function() {
    if($('.gallery').length > 0 && $('.gallery-nav').length > 0) {
        $('.gallery').slick({
            dots: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            asNavFor: '.gallery-nav',
            arrows: true
        });
        $('.gallery-nav').slick({
            dots: false,
            slidesToShow: 9,
            asNavFor: '.gallery',
            focusOnSelect: true,
            slidesToScroll: 1,
        });
    }

    if($('.square-range').length > 0) {
        $('.square-range').ionRangeSlider({
            min: 25,
            max: 654,
            from: 40,
            postfix: ' кв. м.'
        });
    }

    if($('#check-in').length > 0 && $('#check-out').length > 0) {
        $('#check-in').datepicker({minDate: new Date()});
        $('#check-out').datepicker({minDate: new Date()});
    }

    if($('.datetime-pick').length > 0) {
        $('.datetime-pick').datepicker({minDate: new Date(), timepicker: true});
    }

    if($('.pickdate').length > 0) {
        $('.pickdate').datepicker();
    }

    if($('.daterange').length > 0) {
        $('.daterange').datepicker({
            minDate: new Date(),
            range: true,
            multipleDatesSeparator: " - "
        });
    }

    if($('select').length > 0) {
        $('select').selectize();
    }

    $('.menu').click(function () {
        $('#mobile-nav').fadeToggle(500);
    })

    $('.close').click(function () {
        $(this).parent().fadeOut(500);
    });
});