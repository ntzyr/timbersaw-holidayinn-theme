window.Vue = require('vue')

Vue.component('files', require('./metaboxes/Files').default)
Vue.component('file', require('./metaboxes/File').default)
Vue.component('IconTextGroup', require('./metaboxes/IconTextGroup').default)
Vue.component('FeaturesGroup', require('./metaboxes/FeaturesGroup').default)
Vue.component('InputGroup', require('./metaboxes/InputGroup').default)

window.onload = function () {
    const app = new Vue({
        el: "#poststuff"
    })
}
