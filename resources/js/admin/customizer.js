window.Vue = require('vue')

Vue.component('text-blocks', require('./customizer/controls/TextBlocks').default)

window.onload = function () {
    const app = new Vue({
        el: ".vue-customizer"
    })
}