<?php
/**
 * Created by PhpStorm.
 * User: ntzyr
 * Date: 2019-03-21
 * Time: 22:43
 */

require "vendor/autoload.php";

use \Timber\Timber;

Timber::$dirname = array('resources/views/');

new \TimberSaw\Site;

\TimberSaw\Core\Loader::assets();
\TimberSaw\Core\Loader::restRoutes();
\TimberSaw\Core\Loader::customizer();
//\TimberSaw\Core\Loader::customizerControls();
\TimberSaw\Core\Loader::posttypes();