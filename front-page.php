<?php
/**
 * Created by PhpStorm.
 * User: ntzyr
 * Date: 2019-03-29
 * Time: 14:22
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

$context['rooms'] = Timber::get_posts(array(
    'post_type' => 'room'
));

$context['front_features'] = $post->custom['page_features']['features'];

Timber::render( array( '/front/page-' . $post->post_name . '.twig', 'front/front-page.twig', 'front/page.twig' ), $context );