<?php
/**
 * Created by PhpStorm.
 * User: ntzyr
 * Date: 2019-03-29
 * Time: 14:21
 */


$templates = array( '/front/search.twig', '/front/archive.twig', '/front/index.twig' );

$context          = Timber::get_context();
$context['title'] = 'Search results for ' . get_search_query();
$context['posts'] = new Timber\PostQuery();

Timber::render( $templates, $context );