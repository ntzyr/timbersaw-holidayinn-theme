<?php
/**
 * Created by PhpStorm.
 * User: ntzyr
 * Date: 2019-03-21
 * Time: 22:43
 */

/**
 * TODO
// * - wp customizer
 * - loader
 */

$context = Timber::get_context();
$context['posts'] = new Timber\PostQuery();
$context['categories'] = Timber::get_terms('category', array(
    'hide_empty' => true
));
$templates = array('/front/index.twig');
if (is_home()) {
    array_unshift($templates, '/front/home.twig');
}

Timber::render($templates, $context);