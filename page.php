<?php
/**
 * Created by PhpStorm.
 * User: ntzyr
 * Date: 2019-03-29
 * Time: 14:17
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
if(isset($post->custom['page_additional']['gallery'])) {
    foreach( $post->custom['page_additional']['gallery'] as $slide) {
        $context['gallery'][] = new Timber\Image($slide);
    }
}

if(isset($post->custom['page_additional']['files'])) {
    $context['files'] = $post->custom['page_additional']['files'];
}

Timber::render( array( '/front/page-' . $post->post_name . '.twig', '/front/page.twig' ), $context );
